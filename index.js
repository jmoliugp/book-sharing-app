import { AppRegistry } from 'react-native';
import { App } from './src/scenes/app';

AppRegistry.registerComponent('book_sharing', () => App);
