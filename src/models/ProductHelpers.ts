import currencyFormatter from '../helpers/CurrencyFormatter';
import { IProduct } from './';

const getFormattedPrice = (product: IProduct) => {
  return currencyFormatter(product.price);
};

const getFormatedQtyPrice = (product: IProduct, qty: number) => {
  return currencyFormatter(qty * product.price);
};

export default {
  getFormattedPrice,
  getFormatedQtyPrice,
};
