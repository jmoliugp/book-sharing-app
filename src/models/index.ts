export interface IProduct {
  id: string
  name: string
  description: string
  price: number
  photos: string[]
}

export interface ICategory {

}
