import React from 'react';
import { BooksSearch } from '../booksSearch'
import { MyBooks } from '../myBooks'
import { Reviews } from '../reviews'
import { Notifications } from '../notifications'

import listIcon from '../../../assets/booksSearch/listIcon.png';
import myBooksIcon from '../../../assets/myBooks/myBooksIcon.png';
import reviewsIcon from '../../../assets/reviews/reviewsIcon.png';
import notificationsIcon from '../../../assets/notifications/notificationsIcon.png';

export const navigationRoutes = {
  BooksSearch: { screen: BooksSearch },
  MyBooks: { screen: MyBooks },
  Reviews: { screen: Reviews },
  Notifications: { screen: Notifications },
};

export const screensIcons = {
  BooksSearch: listIcon,
  MyBooks: myBooksIcon,
  Reviews: reviewsIcon,
  Notifications: notificationsIcon,
};
