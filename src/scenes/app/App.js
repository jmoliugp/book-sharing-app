import React, { Component } from 'react';
import { Image } from 'react-native';
import { TabNavigator, TabBarBottom } from 'react-navigation';
import { navigationRoutes, screensIcons } from './NavigatorRoutes';

const AppTabNavigator = TabNavigator(
  navigationRoutes,
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        debugger;
        return (
          <Image
            style={{ height: 32, width: 32 }}
            source={screensIcons[routeName]}
            tintColor={tintColor}
          />
        );
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
    animationEnabled: false,
    swipeEnabled: false,
  },
);

export default class App extends Component {
  render() {
    // disable YellowBox warnings
    console.disableYellowBox = true;
    return(
        <AppTabNavigator />
    );
  }
}
